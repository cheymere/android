package fr.example.projet.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import fr.example.projet.R;
import fr.example.projet.data.Stub;
import fr.example.projet.modele.Depenses;

public class DetailsActivite extends AppCompatActivity {

    private final Depenses laDepense = Stub.load();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activite);
    }
}
