package fr.example.projet.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import fr.example.projet.R;

public class ActivitePrincipale extends AppCompatActivity {

    private ProgressBar progressBar;
    private EditText editTextEmail, editTextPass;

    private FirebaseAuth mAuth;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    //reference à la base de données
    DatabaseReference myRef = database.getReference();

    private static final String TAG = "EmailPassword";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activite_principale);

        progressBar = (ProgressBar) findViewById(R.id.progressBare);
        editTextEmail = (EditText) findViewById(R.id.editTextEmailAddress);
        editTextPass = (EditText) findViewById(R.id.editTextPassword);

        mAuth =FirebaseAuth.getInstance();
    }
    @Override
    protected void onStart(){
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //if(currentUser != null) {
            //return;
        //}
    }

    public void signIn(View view) {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPass.getText().toString().trim();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("SignInFailed", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("SignInFailed", "signInWithEmail:failure", task.getException());
                            Toast.makeText(ActivitePrincipale.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });

        progressBar.setVisibility(View.VISIBLE);
    }

    private void updateUI(FirebaseUser user) {
        if(user != null){
            Intent connex = new Intent(this,SecondeActivite.class);
            startActivity(connex);
        }
        else {
            editTextEmail.setError("Email & password are required");
            //if (Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    private void signOut() {
        mAuth.signOut();
        updateUI(null);
    }
}
