package fr.example.projet.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import fr.example.projet.R;
import fr.example.projet.data.Stub;
import fr.example.projet.modele.Budget;
import fr.example.projet.modele.Depenses;
import fr.example.projet.view.adapter.MonAdapter;

public class SecondeActivite extends AppCompatActivity {

    private EditText editTextBudget;
    private Button buttonVal;

    private final Depenses laDepense = Stub.load();
    //private final Budget leBudget = Stub.initial();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    //reference à la base de données
    DatabaseReference myRef = database.getReference();
    DatabaseReference mbudgetRef = myRef.child("budget");

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seconde_activite);

        RecyclerView laRecyclerView = findViewById(R.id.laRecyclerView);
        laRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        laRecyclerView.setAdapter(new MonAdapter(laDepense.getLesDepenses()));

        editTextBudget = findViewById(R.id.budgetMois);
        buttonVal = (Button)findViewById(R.id.buttonValidate);
        //budgetMois.setText(leBudget.getTotalBudget());

        //myRef.setValue("test");
        /*
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //String value = snapshot.getValue(String.class);
                int valueBudget = snapshot.getValue(int.class);
                Log.d("ACHAT", "Total budget : " + valueBudget);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("fail", "Failed to read value", error.toException());
            }
        });

         */

        buttonVal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newBudget();
            }
        });
    }
    @Override
    protected void onStart(){
        super.onStart();
        /*
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String text = snapshot.getValue(String.class);
                editTextBudget.setText(text);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

         */
    }

    private void newBudget(){
        String bud = editTextBudget.getText().toString();

        if(bud.isEmpty()){
            editTextBudget.setError("A budget is required");
            editTextBudget.requestFocus();
        } else {
            //String id = mbudgetRef.push().getKey();
            mbudgetRef.push();

            Budget b = new Budget(bud);
            mbudgetRef.setValue(b);
        }
    }
    /*
    private void registerBudget(){
        String budget = editTextBudget.getText().toString().trim();

        if(budget.isEmpty()){
            editTextBudget.setError("A budget is required");
            editTextBudget.requestFocus();
        }
    }

     */

    public void ajouterAchat(String nom, int achat){
        Intent add = new Intent(this, NewAchatActivite.class);
        startActivity(add);
    }

    public void afficherDetails(View view) {
        Intent det = new Intent(this, DetailsActivite.class);
        startActivity(det);
    }

    public void logout(){
        FirebaseAuth.getInstance().signOut();
    }
}
