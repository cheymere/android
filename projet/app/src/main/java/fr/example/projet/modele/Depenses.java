package fr.example.projet.modele;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Depenses {
    private List<Achat> lesDepenses = new ArrayList<>();

    public List<Achat> getLesDepenses(){
        return Collections.unmodifiableList(lesDepenses);
    }

    public void addAchat(String nom, int prix){
        lesDepenses.add(new Achat(nom, prix));
    }
}
