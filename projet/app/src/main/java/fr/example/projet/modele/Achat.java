package fr.example.projet.modele;

public class Achat {
    private  String nom;
    private int prix;
    private String type;

    public Achat(String nom, int prix){
        this.nom = nom;
        this.prix = prix;
    }

    public String getNom(){
        return nom;
    }

    public void setNom(){
        this.nom = nom;
    }

    public int getPrix(){
        return prix;
    }

    public void setPrix(){
        this.prix = prix;
    }
}
