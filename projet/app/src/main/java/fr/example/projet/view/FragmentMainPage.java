package fr.example.projet.view;

import androidx.fragment.app.Fragment;

import fr.example.projet.R;

public class FragmentMainPage extends Fragment {
    public FragmentMainPage(){
        super(R.layout.fragment_main_page);
    }
}
