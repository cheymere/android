package fr.example.projet.view.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import fr.example.projet.R;

public class MonViewHolder extends RecyclerView.ViewHolder {

    private final TextView textViewAchat;

    public MonViewHolder(@NonNull View itemView) {

        super(itemView);
        textViewAchat = itemView.findViewById(R.id.achat);
    }

    public TextView getTextViewAchat(){
        return textViewAchat;
    }
}
