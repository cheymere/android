package fr.example.projet.modele;

public class Utilisateur {
    private String email;
    private String password;

    public Utilisateur(){}

    public Utilisateur(String login, String password){
        this.email = login;
        this.password = password;
    }

    public String getEmail(){
        return email;
    }

    public String getPassword() {
        return password;
    }
}
