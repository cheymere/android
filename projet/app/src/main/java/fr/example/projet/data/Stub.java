package fr.example.projet.data;

import fr.example.projet.modele.Budget;
import fr.example.projet.modele.Depenses;

public class Stub {

    public static Depenses load(){
        Depenses a = new Depenses();
        a.addAchat("courses", 50);
        a.addAchat("tv", 2000);
        a.addAchat("essence", 40);
        a.addAchat("habits", 150);
        return a;
    }
}
