package fr.example.projet.modele;

public class Budget {
    private String totalBudget;

    //constructeur vide pour base de données
    public Budget(){
    }

    public Budget(String total){
        this.totalBudget = total;
    }

    public String getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(String totalBudget) {
        this.totalBudget = totalBudget;
    }
}
