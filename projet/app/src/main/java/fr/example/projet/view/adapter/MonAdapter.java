package fr.example.projet.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.example.projet.R;
import fr.example.projet.modele.Achat;

public class MonAdapter extends RecyclerView.Adapter {

    private List<Achat> lesAchats;

    public MonAdapter(List<Achat> lesAchats){
        this.lesAchats = lesAchats;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout layoutAchats = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.cellule_achat, parent, false);
        return new MonViewHolder(layoutAchats);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((MonViewHolder)holder).getTextViewAchat().setText(lesAchats.get(position).getNom() + " " + lesAchats.get(position).getPrix());
    }

    @Override
    public int getItemCount() {
        return lesAchats.size();
    }
}
